const cookieParser = require("cookie-parser");
const bodyParser = require('body-parser');
const express = require('express')
const app = express()
const Pool = require("pg").Pool;
const helmet = require("helmet");
const cors = require("cors");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);

require('dotenv/config');

const store = new MongoDBStore({
    uri:
    "mongodb://" +
    process.env.MONGO_USERS +
    ":" +
    process.env.MONGO_PASSWORD +
    "@" +
    process.env.MONGO_HOST +
    "/" +
    process.env.MONGO_DB,
});
  
const corsOptions = {
    credentials: true,
    origin: process.env.ORIGIN_URL,
    methods: "GET, POST, PUT, DELETE",
    allowedHeaders: "Content-Type, Cache-Control",
};

app.disable("x-powered-by");
app.use(cors(corsOptions));
app.use(helmet.xssFilter());
app.use(helmet());
app.use(cookieParser());
app.use(bodyParser.json({ limit: "25mb" }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "25mb",
  })
);

const oneDay = 1000 * 60 * 60 * 24;
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    cookie: { maxAge: oneDay },
    resave: false,
    saveUninitialized: false,
    sameSite: "none",
    store: store,
  })
);

const sdtcon = new Pool({
    user: process.env.SDT_USER,
    host: process.env.SDT_HOST,
    database: process.env.SDT_DB,
    password: process.env.SDT_PASS,
    port: process.env.SDT_PORT,
});

sdtcon.connect(function (err) {
    if (err) throw err;
    console.log("SysDevTracker Database connected successfully");
})

app.post('/sdt/login', function (req, res) {
    const EMAIL = req.body.email;
    const PASS = req.body.password;
    sdtcon.query(`SELECT * FROM public.tblx_users 
    WHERE user_email = $1 AND user_password =$2 
    AND user_status = 'Active'`, 
    [EMAIL, PASS], function(err, results, fields){
    if (results.rowCount >= 1) {
        req.session.userid = results.rows[0].user_id;
        req.session.lname = results.rows[0].user_lname;
        req.session.fname = results.rows[0].user_fname;
        req.session.mname = results.rows[0].user_mname;
        req.session.email = results.rows[0].user_email;
        req.session.empno = results.rows[0].user_empno;
        req.session.status = results.rows[0].user_status;
        req.session.position = results.rows[0].user_position;
        req.session.image = results.rows[0].user_image;
        req.session.color = results.rows[0].user_color;
        req.session.password = results.rows[0].user_password;
        req.session.base64 = results.rows[0].user_b64;
        console.log(req.session)
        res.status(200).json({response: "Ok"});
    } 
    else {
        res.status(401).json({ response: "Error Message: Information does not exist" });
    }
    });
});

app.get('/sdt/checksession', function (req, res) {
    if (!req.session.email) {
        res.status(200).json({ response: "notLoggedIn" });
    } 
    else {
        const datasession = { 
            response: "LoggedIn",
            fullname: req.session.lname + ", " + req.session.fname + " "+ req.session.mname,
            userid: req.session.userid,
            email: req.session.email,
            empno: req.session.empno,
            status: req.session.status,
            position: req.session.position,
            image: req.session.image,
            base64: req.session.base64,
            color: req.session.color,
            password: req.session.password,
        };
        res.status(200).json( datasession );
    }
});

app.get('/sdt/logout', function (req, res) {
    req.session.destroy();
    res.status(200).json({ responsecode: "0934" });
});

app.get('/sdt/users', function (req, res) {
    sdtcon.query(`SELECT * FROM public.tblx_users`, function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

// ----------------------------------------- LOAD DATATABLE USERS -----------------------------------------
app.post('/sdt/userscount', function (req, res) {
    sdtcon.query(`SELECT COUNT(*) FROM public.tblx_users`, function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

app.post('/sdt/userscountlike', function (req, res) {
    const ITEM = '%'+req.body.item+'%';
    sdtcon.query(`SELECT COUNT(*) FROM public.tblx_users 
    WHERE user_lname ILIKE $1`, [ITEM], function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

app.post('/sdt/usersset1', function (req, res) {
    const OFFSET = req.body.offset;
    const LIMIT = req.body.limit;
    sdtcon.query(`SELECT * FROM public.tblx_users 
    OFFSET $1 LIMIT $2`,
    [OFFSET, LIMIT], function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

app.post('/sdt/usersset2', function (req, res) {
    const OFFSET = req.body.offset;
    const LIMIT = req.body.limit;
    const ITEM = '%'+req.body.item+'%';
    sdtcon.query(`SELECT * FROM public.tblx_users 
    WHERE user_lname ILIKE $1 
    OFFSET $2 LIMIT $3`,
    [ITEM, OFFSET, LIMIT], function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});
// ----------------------------------------- LOAD DATATABLE USERS -----------------------------------------

app.post('/sdt/postuser', function (req, res) {
    const LNAME = req.body.lname;
    const FNAME = req.body.fname;
    const MNAME = req.body.mname;
    const EMPNO = req.body.empno;
    const POSITION = req.body.position;
    const EMAIL = req.body.email;
    const PASS = req.body.password;
    const COLOR = req.body.color;
    sdtcon.query(`INSERT INTO public.tblx_users 
    (user_lname, user_fname, user_mname, user_email, 
    user_empno, user_password, 
    user_color, user_position) 
    VALUES 
    ($1, $2, $3, $4, $5, $6, $7, $8)`, 
    [LNAME, FNAME, MNAME, EMAIL, EMPNO, PASS, COLOR, POSITION], function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

app.post('/sdt/postprofile', function (req, res) {
    const USERID = req.body.userid;
    const EMAIL = req.body.email;
    const PASS = req.body.password;
    const PICTURE = req.body.picture;
    const BASE64 = req.body.base64;
    const COLOR = req.body.color;
    sdtcon.query(`UPDATE public.tblx_users SET user_email = $1, 
    user_password = $2, user_image = $3, user_b64 = $4, user_color = $5 
    WHERE user_id = $6`, 
    [EMAIL, PASS, PICTURE, BASE64, COLOR, USERID], function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

app.get('/sdt/log', function (req, res) {
    sdtcon.query(`SELECT * FROM public.tblx_log 
    LEFT JOIN public.tblx_users 
    ON public.tblx_users.user_id::text = public.tblx_log.user_id`, function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

app.get('/sdt/logdashboard', function (req, res) {
    sdtcon.query(`SELECT * FROM 
    (SELECT DISTINCT ON (user_id) * FROM public.tblx_log 
    WHERE log_date = CURRENT_DATE::text 
    ORDER BY user_id, to_char(log_time::time, 'HH:MI:SS am') desc) c 
	RIGHT JOIN (SELECT user_id, user_lname, user_fname, user_mname, 
    user_email, user_image, user_color, user_b64 
    FROM public.tblx_users 
    GROUP BY user_id, user_lname, user_fname, user_mname, 
    user_email, user_image, user_color, user_b64) a 
	ON a.user_id::text = c.user_id 
	ORDER BY user_lname`, function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

app.get('/sdt/logdelete', function (req, res) {
    sdtcon.query(`DELETE FROM public.tblx_log 
    WHERE log_date::date < (CURRENT_DATE - INTERVAL '60 DAYS')::date`, function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

app.post('/sdt/postlog', function (req, res) {
    const USERID = req.body.userid;
    const LOGLOC = req.body.location;
    const LOGDATE = req.body.date;
    const LOGTIME = req.body.time;
    const LOGREMARKS = req.body.remarks;
    sdtcon.query(`INSERT INTO public.tblx_log 
    (user_id, log_location, log_date, log_time, log_remarks) 
    VALUES 
    ($1, $2, $3, $4, $5)`, 
    [USERID, LOGLOC, LOGDATE, LOGTIME, LOGREMARKS], function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

// ----------------------------------------- LOAD DATATABLE LOGS -----------------------------------------
app.post('/sdt/logcount', function (req, res) {
    sdtcon.query(`SELECT COUNT(*) FROM public.tblx_log 
    LEFT JOIN public.tblx_users 
    ON public.tblx_users.user_id::text = public.tblx_log.user_id`, function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

app.post('/sdt/logcountlike', function (req, res) {
    const ITEM = '%'+req.body.item+'%';
    sdtcon.query(`SELECT COUNT(*) FROM public.tblx_log 
    LEFT JOIN public.tblx_users 
    ON public.tblx_users.user_id::text = public.tblx_log.user_id 
    WHERE user_lname ILIKE $1`, [ITEM], function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

app.post('/sdt/logset1', function (req, res) {
    const OFFSET = req.body.offset;
    const LIMIT = req.body.limit;
    sdtcon.query(`SELECT * FROM public.tblx_log 
    LEFT JOIN public.tblx_users 
    ON public.tblx_users.user_id::text = public.tblx_log.user_id 
    OFFSET $1 LIMIT $2`,
    [OFFSET, LIMIT], function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});

app.post('/sdt/logset2', function (req, res) {
    const OFFSET = req.body.offset;
    const LIMIT = req.body.limit;
    const ITEM = '%'+req.body.item+'%';
    sdtcon.query(`SELECT * FROM public.tblx_log 
    LEFT JOIN public.tblx_users 
    ON public.tblx_users.user_id::text = public.tblx_log.user_id 
    WHERE user_lname ILIKE $1 
    OFFSET $2 LIMIT $3`,
    [ITEM, OFFSET, LIMIT], function(err, results, fields){
    if (err) console.log(err);
    res.status(200).json(results);
    });
});
// ----------------------------------------- LOAD DATATABLE LOGS -----------------------------------------

app.listen(process.env.PORT_RUNNER, () => {console.log("Server started at port "+process.env.PORT_RUNNER)})
module.exports = app;